-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2023 at 11:59 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_apple_music`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_pesanan`
--

CREATE TABLE `detail_pesanan` (
  `id` int(11) NOT NULL,
  `pesanan_id` int(11) DEFAULT NULL,
  `kItem_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kategori_produk`
--

CREATE TABLE `kategori_produk` (
  `id` int(11) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL,
  `deskripsi` varchar(50) DEFAULT NULL,
  `imageUrl` varchar(255) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `kategori_produk`
--

INSERT INTO `kategori_produk` (`id`, `nama_kategori`, `deskripsi`, `imageUrl`, `status`) VALUES
(13, 'Drum', 'Drum', 'uploads\\f648dde4-6559-4f85-8449-fab9d9e15b41.png', b'1'),
(14, 'Piano', 'Piano', 'uploads\\29122e90-056f-47ca-91a4-1baef231e84f.png', b'1'),
(15, 'Gitar', 'Gitar', 'uploads\\5bf25fed-49cc-45f6-ad76-e26a9ab9ae13.png', b'1'),
(16, 'Bass', 'Bass', 'uploads\\63488f0b-f8ad-450e-9e9f-1964e5661af6.png', b'1'),
(17, 'Biola', 'Biola', 'uploads\\3ec720f0-463c-41bb-adc9-38fa93149c1d.png', b'1'),
(18, 'Menyanyi', 'Menyanyi', 'uploads\\25798552-7a8e-40c6-b6ee-12297c6e53bd.png', b'1'),
(19, 'Flute', 'Flute', 'uploads\\6d2fc8ce-69a3-4ba8-823d-30f9914fd860.png', b'1'),
(20, 'Saxophone', 'Saxophone', 'uploads\\6e45b69f-e425-4325-8fe3-431da810f1c9.png', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `keranjang_item`
--

CREATE TABLE `keranjang_item` (
  `id` int(11) NOT NULL,
  `schedule` date NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1',
  `pengguna_id` int(11) DEFAULT NULL,
  `produk_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `metode_pembayaran`
--

CREATE TABLE `metode_pembayaran` (
  `id` int(11) NOT NULL,
  `nama_metode` varchar(20) NOT NULL,
  `imageUrl` varchar(255) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `metode_pembayaran`
--

INSERT INTO `metode_pembayaran` (`id`, `nama_metode`, `imageUrl`, `status`) VALUES
(1, 'agus', 'uploads\\7159a34e-ccf7-4800-b0b2-6d6b3f2018de.png', b'1'),
(2, 'agus', 'uploads\\fc95526a-7e46-46cd-be07-842e4ab82b7a.png', b'1'),
(3, 'DANA', 'uploads\\0c57fe63-2b7e-4ad3-944b-5f8dfc039bb7.png', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(200) NOT NULL,
  `role` varchar(10) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1',
  `reset_password_token` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id`, `nama`, `email`, `password`, `role`, `status`, `reset_password_token`) VALUES
(11, 'irsyad', 'irsyadmuhammad001@gmail.com', 'E0C9035898DD52FC65C41454CEC9C4D2611BFB37', 'admin', b'1', '48205CB45E22059925B6822787CF18CB085B16DA'),
(45, 'apel', 'apel@gmail.com', '414767634010A8DDB3B32C37617C2EB935747940', 'admin', b'1', NULL),
(48, 'string', 'string', 'ECB252044B5EA0F679EE78EC1A12904739E2904D', 'admin', b'1', NULL),
(58, '', 'irsyadmuhammad003@gmail.com', '6B6277AFCB65D33525545904E95C2FA240632660', 'admin', b'1', '60C3E73DCC6A3B8D870C0ED28C89ED8D2009E88E');

-- --------------------------------------------------------

--
-- Table structure for table `pesanan`
--

CREATE TABLE `pesanan` (
  `id` int(11) NOT NULL,
  `metode_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `deskripsi_produk` varchar(50) DEFAULT NULL,
  `harga` int(10) NOT NULL,
  `imageUrl` varchar(255) NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1',
  `kategori_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id`, `nama_produk`, `deskripsi_produk`, `harga`, `imageUrl`, `status`, `kategori_id`) VALUES
(32, 'Drum', 'Kursus Drummer Special Coach (Eno Netral)', 850000, 'uploads\\7eee57f9-b13e-4a76-bee6-ceeee93250b7.png', b'1', 13),
(33, 'Gitar', '[Beginner] Guitar class for kids', 1600000, 'uploads\\7b83ee8c-d907-46c2-8376-13983cc4bf23.png', b'1', 15),
(34, 'Biola', 'Biola Mid-Level Course', 3000000, 'uploads\\65e9968b-0708-47ae-bb01-4b5f6f8c456f.png', b'1', 17),
(35, 'Drum', 'Drummer for kids (Level Basic/1)', 2200000, 'uploads\\3be2b062-0357-46aa-ab2d-ad25f28614cc.png', b'1', 13),
(36, 'Piano', 'Kursu Piano : From Zero to Pro (Full Package)', 11650000, 'uploads\\c86d4dfe-0fc6-45c4-a03f-b01e5b0c9ab6.png', b'1', 14),
(37, 'Saxophone', 'Expert Level Saxophone', 7350000, 'uploads\\1a5b3a0d-e8f5-4755-8efa-dc26426df691.png', b'1', 20);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_pesanan`
--
ALTER TABLE `detail_pesanan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pesanan_id` (`pesanan_id`),
  ADD KEY `kItem_id` (`kItem_id`);

--
-- Indexes for table `kategori_produk`
--
ALTER TABLE `kategori_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `keranjang_item`
--
ALTER TABLE `keranjang_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pengguna_id` (`pengguna_id`),
  ADD KEY `produk_id` (`produk_id`);

--
-- Indexes for table `metode_pembayaran`
--
ALTER TABLE `metode_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesanan`
--
ALTER TABLE `pesanan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `metode_id` (`metode_id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kategori_id` (`kategori_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_pesanan`
--
ALTER TABLE `detail_pesanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kategori_produk`
--
ALTER TABLE `kategori_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `keranjang_item`
--
ALTER TABLE `keranjang_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `metode_pembayaran`
--
ALTER TABLE `metode_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `pesanan`
--
ALTER TABLE `pesanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_pesanan`
--
ALTER TABLE `detail_pesanan`
  ADD CONSTRAINT `detail_pesanan_ibfk_1` FOREIGN KEY (`pesanan_id`) REFERENCES `pesanan` (`id`),
  ADD CONSTRAINT `detail_pesanan_ibfk_2` FOREIGN KEY (`kItem_id`) REFERENCES `keranjang_item` (`id`);

--
-- Constraints for table `keranjang_item`
--
ALTER TABLE `keranjang_item`
  ADD CONSTRAINT `keranjang_item_ibfk_1` FOREIGN KEY (`pengguna_id`) REFERENCES `pengguna` (`id`),
  ADD CONSTRAINT `keranjang_item_ibfk_2` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`id`);

--
-- Constraints for table `pesanan`
--
ALTER TABLE `pesanan`
  ADD CONSTRAINT `pesanan_ibfk_1` FOREIGN KEY (`metode_id`) REFERENCES `metode_pembayaran` (`id`);

--
-- Constraints for table `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `produk_ibfk_1` FOREIGN KEY (`kategori_id`) REFERENCES `kategori_produk` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
